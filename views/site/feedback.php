<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

    <h1>Форма обратной связи</h1>

<?php $form = ActiveForm::begin(['options' => ['id' => 'testForm']])?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'email')->input('email') ?>
<?= $form->field($model, 'telephone') ?>
<?= $form->field($model, 'text')->textarea(['rows'=> 10]) ?>

<?= Html::submitButton("Отправить", ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end()?>

<?php if (Yii::$app->session->hasFlash('success') ) : ?>

<?php endif; ?>