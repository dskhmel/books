<?php


namespace app\models;

use yii\db\ActiveRecord;


class FeedbackForm extends ActiveRecord {

//    public $name;
//    public $email;
//    public $text;

    public static function tableName()
    {
        return 'feedback';
    }

    public function attributeLabels()  {
        return [
          'name' => 'Имя',
          'email' => 'E-mail',
          'telephone' => 'Телефон',
          'text' => 'Текст сообщения'
        ];
    }

    public function rules() {
        return [
            [ ['text', 'email'], 'required', ],
            ['email', 'email'],
            ['name', 'trim' ],
            ['telephone', 'trim' ],
            ['text', 'trim']
        ];
    }

//    public function myRule($attr) {
//        if(!in_array($this->$attr, ['hello', 'world'])) {
//            $this->addError($attr, 'Wrong!');
//        }
//    }
}