<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m210704_161527_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'isbn' => $this->text(),
            'pageCount' => $this->integer(),
            'publishedDate' => $this->dateTime(),
            'thumbnailUrl' => $this->text(),
            'shortDescription' => $this->text(),
            'longDescription'  => $this->text(),
            'status'  => $this->text(),
            'authors'  => $this->text(),
        ]);
        $this->alterColumn('books', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
