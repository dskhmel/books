<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books_categories}}`.
 */
class m210704_165311_create_books_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $this->createTable('{{%books_categories}}', [
            'id' => $this->primaryKey(),
            'id_books' => $this->smallInteger(8),
            'id_categories' => $this->smallInteger(8),
        ]);
        $this->alterColumn('books_categories', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');

        $this->addForeignKey('chain_to_books',
                            'books_categories',
                            'id_books',
                            'books',
                            'id',
                            'CASCADE'
        );

        $this->addForeignKey('chain_to_categories',
                            'books_categories',
                            'id_categories',
                            'categories',
                            'id',
                            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books_categories}}');
    }
}
